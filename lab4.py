import traceback
import re


decisionTree = {}

def getException(traceback):
    fileName = "main.py"
    line = re.search("line \d+", traceback.split("\n")[1]).group(0)[5:]
    exception = traceback.split("\n")[-2].strip()
    print("Ошибка в строке", line,  decisionTree[fileName + ' ' + line][exception][hash(traceback)])

if __name__ == '__main__':
    errorList = [
                    ["main.py 26", "TimeoutError", "Traceback (most recent call last):\n  File \"main.py\", line 26, in <module>\n    raise TimeoutError()\nTimeoutError\n", "Время ожидания истекло"],
                    ["main.py 32", "KeyError", "Traceback (most recent call last):\n  File \"main.py\", line 32, in <module>\n    raise KeyError()\nKeyError\n", "Указанный ключ элемента отсуствует"],
                    ["main.py 38", "TypeError", "Traceback (most recent call last):\n  File \"main.py\", line 38, in <module>\n    raise TypeError()\nTypeError\n", "Указанный объект неподдерживает данную операцию, неверный тип объекта"],
                    ["main.py 44", "IndexError", "Traceback (most recent call last):\n  File \"main.py\", line 44, in <module>\n    raise IndexError()\nIndexError\n", "Индекс отсуствует в данном диапазоне"],
                    ["main.py 50", "ImportError", "Traceback (most recent call last):\n  File \"main.py\", line 50, in <module>\n    raise ImportError()\nImportError\n", "Неудачный импорт модуля"]
                ]

    for errorIndex in errorList:
        decisionTree[errorIndex[0]] = {errorIndex[1]:{hash(errorIndex[2]): errorIndex[3]}}

    try:
        raise TimeoutError()

    except:
        getException(traceback.format_exc())

    try:
        raise KeyError()

    except:
        getException(traceback.format_exc())

    try:
        raise TypeError()

    except:
        getException(traceback.format_exc())

    try:
        raise IndexError()

    except:
        getException(traceback.format_exc())

    try:
        raise ImportError()

    except:
        getException(traceback.format_exc())
        